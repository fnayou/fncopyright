/*
 * fnCopyright 
 * :: fork from the awesome https://github.com/snaptortoise/konami-js
 * :: for evident purpose !
 * Code: https://bitbucket.org/aymen_fnayou/fnCopyright
 * Copyright (c) 2014 Aymen FNAYOU (aymen-fnayou.com)
 * Version: 1.0.0 (7/7/2014)
 * Licensed under the MIT License (http://opensource.org/licenses/MIT)
 * Tested in: Safari 4+, Google Chrome 4+, Firefox 3+, IE7+, Mobile Safari 2.2.1 and Dolphin Browser
 */

var fnCopyright = function (callback) {
    var fn_copyright = {
        addEvent: function (obj, type, fn, ref_obj) {
            if (obj.addEventListener)
                obj.addEventListener(type, fn, false);
            else if (obj.attachEvent) {
                // IE
                obj["e" + type + fn] = fn;
                obj[type + fn] = function () {
                    obj["e" + type + fn](window.event, ref_obj);
                }
                obj.attachEvent("on" + type, obj[type + fn]);
            }
        },
        input: "",
        pattern: "707865897985",
        load: function (link) {
            this.addEvent(document, "keydown", function (e, ref_obj) {
                if (ref_obj) fn_copyright = ref_obj; // IE
                fn_copyright.input += e ? e.keyCode : event.keyCode;
                if (fn_copyright.input.length > fn_copyright.pattern.length)
                    fn_copyright.input = fn_copyright.input.substr((fn_copyright.input.length - fn_copyright.pattern.length));
                if (fn_copyright.input == fn_copyright.pattern) {
                    fn_copyright.code(link);
                    fn_copyright.input = "";
                    e.preventDefault();
                    return false;
                }
            }, this);
            this.iphone.load(link);
        },
        code: function (link) {
            window.location = link
        },
        iphone: {
            start_x: 0,
            start_y: 0,
            stop_x: 0,
            stop_y: 0,
            tap: false,
            capture: false,
            orig_keys: "",
            keys: ["LEFT", "TAP", "RIGHT", "TAP"],
            code: function (link) {
                fn_copyright.code(link);
            },
            load: function (link) {
                this.orig_keys = this.keys;
                fn_copyright.addEvent(document, "touchmove", function (e) {
                    if (e.touches.length === 1 && fn_copyright.iphone.capture === true) {
                        var touch = e.touches[0];
                        fn_copyright.iphone.stop_x = touch.pageX;
                        fn_copyright.iphone.stop_y = touch.pageY;
                        fn_copyright.iphone.tap = false;
                        fn_copyright.iphone.capture = false;
                        fn_copyright.iphone.check_direction();
                    }
                });
                fn_copyright.addEvent(document, "touchend", function () {
                    if (fn_copyright.iphone.tap === true) fn_copyright.iphone.check_direction(link);
                }, false);
                fn_copyright.addEvent(document, "touchstart", function (evt) {
                    fn_copyright.iphone.start_x = evt.changedTouches[0].pageX;
                    fn_copyright.iphone.start_y = evt.changedTouches[0].pageY;
                    fn_copyright.iphone.tap = true;
                    fn_copyright.iphone.capture = true;
                });
            },
            check_direction: function (link) {
                x_magnitude = Math.abs(this.start_x - this.stop_x);
                y_magnitude = Math.abs(this.start_y - this.stop_y);
                x = ((this.start_x - this.stop_x) < 0) ? "RIGHT" : "LEFT";
                y = ((this.start_y - this.stop_y) < 0) ? "DOWN" : "UP";
                result = (x_magnitude > y_magnitude) ? x : y;
                result = (this.tap === true) ? "TAP" : result;

                if (result == this.keys[0]) this.keys = this.keys.slice(1, this.keys.length);
                if (this.keys.length == 0) {
                    this.keys = this.orig_keys;
                    this.code(link);
                }
            }
        }
    }

    typeof callback === "string" && fn_copyright.load(callback);
    if (typeof callback === "function") {
        fn_copyright.code = callback;
        fn_copyright.load();
    }

    return fn_copyright;
};
