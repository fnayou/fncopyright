fnCopyright
==============

Author
--------------
-   Aymen Fnayou
-   developer@aymen-fnayou.com
-   http://www.aymen-fnayou.com

About
--------------
-   just a fork of the Konami code plugin https://github.com/snaptortoise/konami-js for evident purpose !

Installation
--------------
include fnCopyright.min.js

    <script type="text/javascript" src="fnCopyright.min.js"></script>

initialize with an url like following

    var copyright = new fnCopyright('http://aymen-fnayou.com');

or using a valid callback

    var copyright = new fnCopyright(function() { alert('I developed this website!')});

Note
--------------
Thanks for George Mandis for his awesome plugin ;)
Tip use theses function to hide some data

    function utf8_to_b64( str ) {
            return window.btoa(unescape(encodeURIComponent( str )));
    }
    function b64_to_utf8( str ) {
            return decodeURIComponent(escape(window.atob( str )));
    }
    utf8_to_b64('http://aymen-fnayou.com'); // "aHR0cDovL2F5bWVuLWZuYXlvdS5jb20="
    b64_to_utf8('aHR0cDovL2F5bWVuLWZuYXlvdS5jb20='); // "http://aymen-fnayou.com"
